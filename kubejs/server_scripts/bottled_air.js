onEvent('item.right_click', event => {
  if (event.item === 'minecraft:glass_bottle' && event.player.y >= 128) {
    event.player.give("kubejs:bottled_air")
    event.item.count -= 1
  }
})
