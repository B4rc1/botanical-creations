onEvent('recipes', event => {
  event.remove({output: 'create:andesite_alloy'})


  event.shaped("create:andesite_alloy", [
    'NR',
    'RN',
  ], {
    N: 'botania:manasteel_nugget',
    R: 'botania:livingrock'
  })


  event.shaped("kubejs:gold_hand", [
      " R ",
      "AAA",
      " A ",
  ], {
    R: "create:andesite_alloy",
    A: "create:golden_sheet"
  })

  event.replaceInput({item: "create:deployer"}, "create:brass_hand", "kubejs:gold_hand")


  event.remove({output:"botania:glimmering_livingwood_log"})
  event.shapeless("botania:glimmering_livingwood_log", [
      "botania:livingwood_log",
      "create:refined_radiance",
  ])
  event.remove({output:"botania:glimmering_stripped_livingwood_log"})
  event.shapeless("botania:glimmering_stripped_livingwood_log", [
      "botania:stripped_livingwood_log",
      "create:refined_radiance",
  ])
  event.remove({output:"botania:glimmering_livingwood"})
  event.shapeless("botania:glimmering_livingwood", [
      "botania:livingwood",
      "create:refined_radiance",
  ])
  event.remove({output:"botania:glimmering_stripped_livingwood"})
  event.shapeless("botania:glimmering_stripped_livingwood", [
      "botania:stripped_livingwood",
      "mcreate:refined_radiance",
  ])


  event.remove({output: 'minecraft:flint_and_steel'})
  event.shapeless("minecraft:flint_and_steel", [
    'botania:rune_fire',
    'botania:terrasteel_ingot',
  ])


  event.remove({output:"create:powdered_obsidian"})
  event.recipes.createCrushing(
      ["create:powdered_obsidian",
      "minecraft:obsidian"],
      "minecraft:obsidian")

  let deploy = event.recipes.createDeploying
  event.recipes.createSequencedAssembly(["kubejs:elemental_rune"], "botania:livingrock_slab",
    [
      deploy("kubejs:unfinished_elemental_rune", ["kubejs:unfinished_elemental_rune", "botania:manasteel_nugget"]),
      deploy("kubejs:unfinished_elemental_rune", ["kubejs:unfinished_elemental_rune", "botania:mana_powder"]),
      deploy("kubejs:unfinished_elemental_rune", ["kubejs:unfinished_elemental_rune", "create:powdered_obsidian"]),
      deploy("kubejs:unfinished_elemental_rune", ["kubejs:unfinished_elemental_rune", "kubejs:bottled_air"]),
      // for source of workaround see: https://github.com/KubeJS-Mods/KubeJS-Create/issues/12
      event.custom({
        type: "create:filling",
        ingredients: [
          Item.of("kubejs:unfinished_elemental_rune").toJson(),
          Fluid.of("minecraft:water", 20250).toJson(), // 250 mB, 8100 * 2.5 = 20250
        ],
        results: [
          Item.of("kubejs:unfinished_elemental_rune").toJson(),
        ],
      }),
      event.custom({
        type: "create:filling",
        ingredients: [
          Item.of("kubejs:unfinished_elemental_rune").toJson(),
          Fluid.of("minecraft:lava", 20250).toJson(), // 250 mB, 8100 * 2.5 = 20250
        ],
        results: [
          Item.of("kubejs:unfinished_elemental_rune").toJson(),
        ],
      }),

    ]).transitionalItem("kubejs:unfinished_elemental_rune").loops(4)


  event.remove({output:"botania:rune_earth"})
  event.recipes.createPressing("botania:rune_earth", "kubejs:elemental_rune")
  event.remove({output:"botania:rune_water"})
  event.recipes.createSplashing("botania:rune_water", "kubejs:elemental_rune")
  event.remove({output:"botania:rune_fire"})
  event.blasting("botania:rune_fire", "kubejs:elemental_rune")
  event.remove({output:"botania:rune_air"})
  event.smoking("botania:rune_air", "kubejs:elemental_rune")


  event.recipes.botania.runic_altar("create:chromatic_compound", [
    "botania:rune_spring",
    "botania:rune_summer",
    "botania:rune_autumn",
    "botania:rune_winter",
    "minecraft:nether_wart",
    "minecraft:nether_wart",
    "minecraft:ender_pearl",
    "minecraft:quartz",
    "minecraft:quartz"
  ], 5000)


  event.recipes.botania.elven_trade("ae2:fluix_crystal",["ae2:certus_quartz_crystal","ae2:certus_quartz_crystal"])
  event.recipes.botania.elven_trade("ae2:calculation_processor_press", ["ae2:printed_silicon", "minecraft:iron_ingot"])
  event.recipes.botania.elven_trade("ae2:engineering_processor_press", ["ae2:printed_silicon", "minecraft:diamond"])
  event.recipes.botania.elven_trade("ae2:logic_processor_press", ["ae2:printed_silicon", "minecraft:gold_ingot"])
  event.recipes.botania.elven_trade("ae2:silicon_press", ["ae2:silicon", "ae2:silicon"])


  event.remove({output:"ae2:quartz_growth_accelerator"})
  event.shaped("ae2:quartz_growth_accelerator", [
    "ABA",
    "CDC",
    "ABA",
    ], {
    A: "minecraft:iron_ingot",
    B: "ae2:fluix_glass_cable",
    C: "ae2:quartz_glass",
    D: "botania:life_essence"
  })



})
