// priority: 0

console.info('Hello, World! (You will only see this line once in console, during startup)')

onEvent('item.registry', event => {
	// Register new items here
	// event.create('example_item').displayName('Example Item')
    event.create("gold_hand").displayName("Gold Hand")

  event.create("bottled_air").displayName("Bottled Air")
    .tooltip(Text.of('Can be Obtained by'))
    .tooltip(Text.of('Rightclicking with'))
    .tooltip(Text.of('a glass bottle above'))
    .tooltip(Text.of('y-level 127'))

  event.create("elemental_rune").displayName("Elemental Rune")
  event.create("unfinished_elemental_rune", "create:sequenced_assembly").displayName("Unfinished Elemental Rune")
})

onEvent('block.registry', event => {
	// Register new blocks here
	// event.create('example_block').material('wood').hardness(1.0).displayName('Example Block')
})
